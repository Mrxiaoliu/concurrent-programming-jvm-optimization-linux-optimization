什么是CAS 
1.对比并交换
2.底层是Unsafe类的方法直接操作内存的数据
3.缺点:cas操作失败，有自旋锁，while循环时间长,占cpu高，有aba问题，且只能保证一个变量原子。

CyclicBarrier和CountDownLatch区别
CountDownLatch计数器减法 一直减到0
主要是await和countDown方法
调用await会阻塞 等到countDown的计数为0，才继续执行
两种用法 一种是一个线程等待一些线程执行完后执行
另一种是多线程在同一时刻执行
关键点在与线程中执行代码是await还是countDown
CyclicBarrier是计数器 加法 从0开始加到创建对象时指定的值
只有一个await方法 等待其他线程都同时执行到await，所有的线程才继续执行，可以写出CountDownLatch
的第一种用法，但CyclicBarrier可以重复使用，而CountDownLatch只能使用一次，减到0，就不能重新使用

volatile关键字的作用
主要解决synchronized的串行化，锁太重
1.保证可见性和防止指令重排保证代码的有序性
2.但不保证原子性 只对boolean修饰的保证原子性

如何正确的使用wait()?使用if还是while？
在synchronized中使用，在使用while里面使用wait

为什么wait、nofity和nofityAll这些方法不放在Thread类当中
wait、nofity和nofityAll是针对锁的阻塞和等待，锁是对象级别的

synchronized和ReentrantLock的区别
1.synchronized是关键字，底层是monitor对象实现。
ReentrantLock是并发包下的，底层是AQS实现。需要手动释放锁
2.使用方法：sync不需要手动释放锁，而Lock需要手动释放。
3.是否可中断：sync不可中断，除非抛出异常或者正常运行完成。Lock是可中断的，通过调用interrupt()方法。
4.是否为公平锁：sync只能是非公平锁，而Lock既能是公平锁，又能是非公平锁。
5.绑定多个条件：sync不能，只能随机唤醒。而Lock可以通过Condition来绑定多个条件，精确唤醒。

什么是AQS
1.抽象队列同步器
2.通过volatile修饰的state int 变量和双向连表和cas实现阻塞队列
继承父类AbstractOwnableSynchronizer 记录持有锁的线程，以及cas工具类实现并发包的核心。

什么是Java内存模型
Java内存模型是一个内存规范，分为主内存和工作内存
每个Java线程都有自己的工作内存。操作数据，首先从主内存中读，得到一份拷贝，操作完毕后再写回到主内存。JMM可能带来可见性、原子性和有序性问题。所谓可见性，就是某个线程对主内存内容的更改，应该立刻通知到其它线程。原子性是指一个操作是不可分割的，不能执行到一半，就不执行了。所谓有序性，就是指令是有序的，不会被重排。

什么是自旋
尝试获取锁的线程不会立即阻塞，而是采用循环的方式去尝试获取。自己在那儿一直循环获取，就像“自旋”一样。这样的好处是减少线程切换的上下文开销，缺点是会消耗CPU。CAS底层的getAndAddInt就是自旋锁思想。
